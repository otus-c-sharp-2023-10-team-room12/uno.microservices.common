﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uno.Microservices.Common.Common
{
    /// <summary>
    /// Базовый ответ
    /// </summary>
    /// <typeparam name="T">Тип данных ответа</typeparam>
    public class Response<T>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public Response() { }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="data">Данные ответа</param>
        /// <param name="message">Сообщение</param>
        public Response(T data, string message)
        {
            Succeeded = true;
            Message = message;
            Data = data;
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="message">Сообщение</param>
        public Response(string message)
        {
            Succeeded = false;
            Message = message;
        }
        /// <summary>
        /// Успешность обработки запроса
        /// </summary>
        public bool Succeeded { get; set; } = true;

        /// <summary>
        /// Дополнительное сообщение об ошибке
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Список ошибки
        /// </summary>
        public List<string> Errors { get; set; }

        /// <summary>
        /// Данные
        /// </summary>
        public T Data { get; set; }
    }
}
