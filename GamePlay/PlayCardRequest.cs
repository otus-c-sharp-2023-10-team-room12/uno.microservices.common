﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uno.Microservices.Common.GamePlay
{
    /// <summary>
    /// Сыграть карту
    /// </summary>
    public class PlayCardRequest
    {
        /// <summary>
        /// Айди пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Айди карты
        /// </summary>
        public int CardId { get; set; }
    }

}
